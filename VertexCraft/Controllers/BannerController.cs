﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VertexCraft.Models;
using VertexCraft.ViewModel;

namespace VertexCraft.Controllers
{
    [Authorize]
    public class BannerController : Controller
    {
        // GET: Banner
        ApplicationDbContext db = new ApplicationDbContext();
        
        public ActionResult Index()
        {
            var category = db.Categories.Where(c => c.IsDeleted == false).ToList();
            var banner = db.Banners.Where(c=> c.IsDelete == false).ToList();
            List<BannerViewModel> bannerViewModels = new List<BannerViewModel>();
            foreach (var item in banner)
            {
                bannerViewModels.Add(new BannerViewModel() {
                    Id = item.Id,
                    Title = item.Title,
                    SubTitle = item.SubTitle,
                    ImagePath = item.ImagePath,
                    Category = category.FirstOrDefault(c=> c.Id == item.CategoryId)
                });
            }
            return View(bannerViewModels);
        }

        // GET: Banner/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var banner = db.Banners.FirstOrDefault(c => c.Id == id);
            var category = db.Categories.ToList();
            BannerViewModel bannerViewModel = new BannerViewModel()
            {
                Id = banner.Id,
                Title = banner.Title,
                SubTitle = banner.SubTitle,
                ImagePath = banner.ImagePath,
                Category = category.FirstOrDefault(c => c.Id == banner.CategoryId)
            };

            return View(bannerViewModel);
        }

        // GET: Banner/Create
        public ActionResult Create()
        {
            var category = db.Categories.Where(c=> c.IsDeleted == false).ToList();
            BannerViewModel bannerViewModel = new BannerViewModel()
            {
                Categories = category
            };
            return View(bannerViewModel);
        }

        // POST: Banner/Create
        [HttpPost]
        public ActionResult Create(BannerViewModel bannerViewModel)
        {
            try
            {

                if(bannerViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(bannerViewModel.Image.FileName);
                    string extension = Path.GetExtension(bannerViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/BannerImages/"), fileNames);
                    bannerViewModel.Image.SaveAs(fileName);

                    Banner banner = new Banner()
                    {
                        ImagePath = path,
                        Title = bannerViewModel.Title,
                        SubTitle = bannerViewModel.SubTitle,
                        CategoryId = bannerViewModel.CategoryId
                    };
                    db.Banners.Add(banner);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    Banner banner = new Banner()
                    {
                        Title = bannerViewModel.Title,
                        SubTitle = bannerViewModel.SubTitle,
                        ImagePath = bannerViewModel.ImagePath,
                        CategoryId = bannerViewModel.CategoryId
                    };
                    db.Banners.Add(banner);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }                
            }
            catch
            {
                return View();
            }
        }

        // GET: Banner/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var banner = db.Banners.FirstOrDefault(c => c.Id == id);
            var category = db.Categories.Where(c => c.IsDeleted == false).ToList();
            BannerViewModel bannerViewModel = new BannerViewModel()
            {
                Id = banner.Id,
                Title = banner.Title,
                SubTitle = banner.SubTitle,
                ImagePath = banner.ImagePath                
            };
            ViewBag.CategoryId = new SelectList(category, "Id", "Name", banner.CategoryId);
            return View(bannerViewModel);
        }

        // POST: Banner/Edit/5
        [HttpPost]
        public ActionResult Edit(BannerViewModel bannerViewModel)
        {
            try
            {
                if (bannerViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(bannerViewModel.Image.FileName);
                    string extension = Path.GetExtension(bannerViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/BannerImages/"), fileNames);
                    bannerViewModel.Image.SaveAs(fileName);

                    Banner banner = new Banner()
                    {
                        Id = bannerViewModel.Id,
                        Title = bannerViewModel.Title,
                        SubTitle = bannerViewModel.SubTitle,
                        ImagePath = path,
                        CategoryId = bannerViewModel.CategoryId
                    };
                    db.Entry(banner).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    Banner banner = new Banner()
                    {
                        Id = bannerViewModel.Id,
                        Title = bannerViewModel.Title,
                        SubTitle = bannerViewModel.SubTitle,
                        ImagePath = bannerViewModel.ImagePath,
                        CategoryId = bannerViewModel.CategoryId
                    };
                    db.Entry(banner).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View();
            }
        }


        public ActionResult Delete(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var banner = db.Banners.FirstOrDefault(c => c.Id == id);
            banner.IsDelete = true;
            db.Entry(banner).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
