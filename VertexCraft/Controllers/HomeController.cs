﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VertexCraft.Models;
using VertexCraft.ViewModel;

namespace VertexCraft.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            HomeViewModel homeViewModel = new HomeViewModel();
            var category = db.Categories.Where(c => c.IsDeleted == false).ToList();
            var banner = db.Banners.Where(c => c.IsDelete == false).OrderByDescending(c=> c.Id).ToList();

            List<BannerViewModel> bannerViewModels = new List<BannerViewModel>();
            foreach (var item in banner)
            {
                bannerViewModels.Add(new BannerViewModel() {
                    Title = item.Title,
                    SubTitle = item.SubTitle,
                    Category = category.FirstOrDefault(c=> c.Id == item.CategoryId),
                    ImagePath = item.ImagePath
                });
            }
            
            var newArrivalProductImage = db.NewArrivals.OrderByDescending(c => c.Id).Skip(0).Take(2).ToList();
            List<NewArrivaleViewModel> newArrivales = new List<NewArrivaleViewModel>();
            foreach (var item in newArrivalProductImage)
            {
                newArrivales.Add(new NewArrivaleViewModel()
                {
                    Id = item.Id,
                    Title = item.Title,
                    FilePath = item.FilePath
                });
            }

            var discountProduct = db.Products.Where(c => c.IsDelete == false && c.Discount > 0).OrderByDescending(c => c.Id).Skip(0).Take(4).ToList();
            List<ProductNameDiscountViewModel> productNameDiscountViewModels = new List<ProductNameDiscountViewModel>();
            foreach (var item in discountProduct)
            {
                var productDiscountImage = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                if(productDiscountImage != null)
                {
                    productNameDiscountViewModels.Add(new ProductNameDiscountViewModel()
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Discount = item.Discount,
                        FilePath = productDiscountImage.ImagePath
                    });
                }
                else
                {
                    productNameDiscountViewModels.Add(new ProductNameDiscountViewModel()
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Discount = item.Discount
                    });
                }

                
            }


            var product = db.Products.Where(c => c.IsDelete == false).OrderByDescending(c => c.Id).Skip(0).Take(4).ToList();
            List<ProductFeatureArrivalsViewModel> productFeatureArrivalsViewModels = new List<ProductFeatureArrivalsViewModel>();
            foreach (var item in product)
            {
                var productDiscountImage = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);

                if(productDiscountImage != null)
                {
                    productFeatureArrivalsViewModels.Add(new ProductFeatureArrivalsViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = productDiscountImage.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productFeatureArrivalsViewModels.Add(new ProductFeatureArrivalsViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }

                
            }

            homeViewModel.BannerViewModels = bannerViewModels;
            homeViewModel.NewArrivales = newArrivales;
            homeViewModel.productNameDiscountViewModels = productNameDiscountViewModels;
            homeViewModel.ProductFeatureArrivalsViewModels = productFeatureArrivalsViewModels;

            return View(homeViewModel);
        }

        public ActionResult ProductDetails(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.Products.FirstOrDefault(c => c.Id == id);
            var cat = db.Categories.FirstOrDefault(c => c.Id == product.CategoryId);
            var productImage = db.ProductImages.Where(c => c.ProductId == product.Id).ToList();
            ProductViewModel productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                Price = product.Price
            };

            ViewBag.ProductImage = productImage;

            var relatedProduct = db.Products.Where(c => c.IsDelete == false && c.Category.Name == cat.Name).OrderByDescending(c => c.Id).ToList();
            List<ProductFeatureArrivalsViewModel> productFeatureArrivalsViewModels = new List<ProductFeatureArrivalsViewModel>();
            foreach (var item in relatedProduct)
            {
                var relatedProductImage = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);

                if(relatedProductImage != null)
                {
                    productFeatureArrivalsViewModels.Add(new ProductFeatureArrivalsViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = relatedProductImage.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productFeatureArrivalsViewModels.Add(new ProductFeatureArrivalsViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }

                
            }

            ViewBag.RelatedProduct = productFeatureArrivalsViewModels;
            return View(productViewModel);
        }


        public ActionResult LeatherBags()
        {
            string leatherBags = "LATHER BAGS";
            var product = db.Products.Where(c => c.IsDelete == false && c.Category.Name == leatherBags).ToList();
            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var item in product)
            {
                var productImages = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                if(productImages != null)
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = productImages.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
            }
            return View(productViewModels);
        }


        public ActionResult JuteFabrics()
        {
            string leatherBags = "JUTE FABRICS";
            var product = db.Products.Where(c => c.IsDelete == false && c.Category.Name == leatherBags).ToList();
            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var item in product)
            {
                var productImages = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                productViewModels.Add(new ProductViewModel
                {
                    Id = item.Id,
                    Title = item.Title,
                    ImagePath = productImages.ImagePath,
                    Price = item.Price,
                    Discount = item.Discount
                });
            }
            return View(productViewModels);
        }

        public ActionResult JuteBags()
        {
            string leatherBags = "JUTE BAGS";
            var product = db.Products.Where(c => c.IsDelete == false && c.Category.Name == leatherBags).ToList();
            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var item in product)
            {
                var productImages = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                if (productImages != null)
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = productImages.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
            }
            return View(productViewModels);
        }

        public ActionResult CottonBags()
        {
            string leatherBags = "COTTON BAGS";
            var product = db.Products.Where(c => c.IsDelete == false && c.Category.Name == leatherBags).ToList();
            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var item in product)
            {
                var productImages = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                if (productImages != null)
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = productImages.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
            }
            return View(productViewModels);
        }

        public ActionResult JuteAndLeatherCrafts()
        {
            string leatherBags = "JUTE & LEATHER CRAFTS";
            var product = db.Products.Where(c => c.IsDelete == false && c.Category.Name == leatherBags).ToList();
            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var item in product)
            {
                var productImages = db.ProductImages.FirstOrDefault(c => c.ProductId == item.Id);
                if (productImages != null)
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        ImagePath = productImages.ImagePath,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
                else
                {
                    productViewModels.Add(new ProductViewModel
                    {
                        Id = item.Id,
                        Title = item.Title,
                        Price = item.Price,
                        Discount = item.Discount
                    });
                }
            }
            return View(productViewModels);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Overview()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Ourprofile()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult MissionVission()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ShopNow(int? id)
        {
            return View();
        }
    }
}