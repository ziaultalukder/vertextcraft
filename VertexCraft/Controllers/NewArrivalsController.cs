﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VertexCraft.Models;
using VertexCraft.ViewModel;

namespace VertexCraft.Controllers
{
    public class NewArrivalsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: NewArrivals
        public ActionResult Index()
        {
            return View(db.NewArrivals.ToList());
        }

        // GET: NewArrivals/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewArrival newArrival = db.NewArrivals.Find(id);
            if (newArrival == null)
            {
                return HttpNotFound();
            }
            return View(newArrival);
        }

        // GET: NewArrivals/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewArrivals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewArrivaleViewModel newArrivalViewModel)
        {
            if (ModelState.IsValid)
            {
                if (newArrivalViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(newArrivalViewModel.Image.FileName);
                    string extension = Path.GetExtension(newArrivalViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/NewArrivalImages/"), fileNames);
                    newArrivalViewModel.Image.SaveAs(fileName);
                    
                    //ResizeSettings imageResizer = new ResizeSettings()
                    //{
                    //    Width = 700,
                    //    Height = 398
                    //};
                    //ImageBuilder.Current.Build(fileName, fileName, imageResizer);

                    NewArrival newArrival = new NewArrival()
                    {
                        FilePath = path,
                        Title = newArrivalViewModel.Title
                    };
                    db.NewArrivals.Add(newArrival);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    NewArrival newArrival = new NewArrival()
                    {
                        FilePath = newArrivalViewModel.FilePath,
                        Title = newArrivalViewModel.Title
                    };
                    db.NewArrivals.Add(newArrival);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            return View();
        }

        // GET: NewArrivals/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var newArrival = db.NewArrivals.FirstOrDefault(c => c.Id == id);
            NewArrivaleViewModel newArrivaleViewModel = new NewArrivaleViewModel()
            {
                Id = newArrival.Id,
                Title = newArrival.Title,
                FilePath = newArrival.FilePath
            };
            return View(newArrivaleViewModel);
        }

        // POST: NewArrivals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(NewArrivaleViewModel newArrivalViewModel)
        {
            if (ModelState.IsValid)
            {
                if (newArrivalViewModel.Image != null)
                {
                    string fileName = Path.GetFileNameWithoutExtension(newArrivalViewModel.Image.FileName);
                    string extension = Path.GetExtension(newArrivalViewModel.Image.FileName);
                    var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
                    fileName = Path.Combine(Server.MapPath("~/NewArrivalImages/"), fileNames);
                    newArrivalViewModel.Image.SaveAs(fileName);
                    

                    //ResizeSettings imageResizer = new ResizeSettings()
                    //{
                    //    Height = 398                        
                    //};
                    

                    //ImageBuilder.Current.Build(fileName, fileName, imageResizer);

                    NewArrival newArrival = new NewArrival()
                    {
                        Id = newArrivalViewModel.Id,
                        FilePath = path,
                        Title = newArrivalViewModel.Title
                    };
                    db.Entry(newArrival).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    NewArrival newArrival = new NewArrival()
                    {
                        Id = newArrivalViewModel.Id,
                        FilePath = newArrivalViewModel.FilePath,
                        Title = newArrivalViewModel.Title
                    };
                    db.Entry(newArrival).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View();
        }

        // GET: NewArrivals/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewArrival newArrival = db.NewArrivals.Find(id);
            if (newArrival == null)
            {
                return HttpNotFound();
            }
            return View(newArrival);
        }

        // POST: NewArrivals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewArrival newArrival = db.NewArrivals.Find(id);
            db.NewArrivals.Remove(newArrival);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
