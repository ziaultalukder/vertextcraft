﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VertexCraft.Models;
using VertexCraft.ViewModel;

namespace VertexCraft.Controllers
{
    [Authorize]
    public class CategoryController : Controller
    {
        // GET: Category
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var category = db.Categories.Where(c=>c.IsDeleted == false).ToList();
            return View(category);
        }

        // GET: Category/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Category/Create
        public ActionResult Create()
        {
            CategoryViewModel categoryViewModel = new CategoryViewModel()
            {
                Categories = db.Categories.Where(c => c.IsDeleted == false).ToList()
            };
            return View(categoryViewModel);
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(CategoryViewModel categoryViewModel)
        {
            try
            {
                Category category = new Category()
                {
                    Name = categoryViewModel.Name,
                    CatId = categoryViewModel.CatId
                };
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Edit/5
        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var category = db.Categories.FirstOrDefault(c => c.Id == id);
            CategoryViewModel categoryViewModel = new CategoryViewModel()
            {
                Id = category.Id,
                Name = category.Name
            };
            ViewBag.CatId = new SelectList(db.Categories.ToList(), "Id", "Name", category.CatId);
            return View(categoryViewModel);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult Edit(CategoryViewModel categoryViewModel)
        {
            try
            {
                Category category = new Category()
                {
                    Id = categoryViewModel.Id,
                    Name = categoryViewModel.Name,
                    CatId = categoryViewModel.CatId
                };

                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Category/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var category = db.Categories.FirstOrDefault(c => c.Id == id );

            category.IsDeleted = true;


            db.Entry(category).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       
    }
}
