﻿using ImageResizer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Web;
using System.Web.Mvc;
using VertexCraft.Models;
using VertexCraft.ViewModel;

namespace VertexCraft.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        // GET: Product
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var product = db.Products.Where(c => c.IsDelete == false).ToList();
            var category = db.Categories.Where(c => c.IsDeleted == false);

            List<ProductViewModel> productViewModels = new List<ProductViewModel>();
            foreach (var data in product)
            {
                productViewModels.Add(new ProductViewModel()
                {
                    Id = data.Id,
                    Title = data.Title,
                    Description = data.Description,
                    Price = data.Price,
                    Discount = data.Discount,
                    Category = category.FirstOrDefault(c=>c.Id == data.CategoryId)
                });
            }
            return View(productViewModels);
        }

        public ActionResult Create()
        {
            var cartegories = db.Categories.Where(c => c.IsDeleted == false);
            ProductViewModel productViewModel = new ProductViewModel()
            {
                Categories = cartegories
            };
            return View(productViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProductViewModel productViewModel)
        {
            Product product = new Product()
            {
                Title = productViewModel.Title,
                Description = productViewModel.Description,
                Price = productViewModel.Price,
                Discount = productViewModel.Discount,
                CategoryId = productViewModel.CategoryId
            };

            db.Products.Add(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var category = db.Categories.ToList();
            var productImage = db.ProductImages.ToList();
            var product = db.Products.FirstOrDefault(c => c.Id == id);
            ProductViewModel productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                Price = product.Price,
                Discount = product.Discount,
                Category = category.FirstOrDefault(c=>c.Id == product.CategoryId)
            };
            return View(productViewModel);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productImage = db.ProductImages.ToList();
            var product = db.Products.FirstOrDefault(c => c.Id == id);

            product.IsDelete = true;
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var category = db.Categories.Where(c=> c.IsDeleted == false).ToList();
            var product = db.Products.FirstOrDefault(c => c.Id == id);

            ProductViewModel productViewModel = new ProductViewModel()
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                Price = product.Price,
                Discount = product.Discount
            };
            ViewBag.CategoryId = new SelectList(category, "Id", "Name", product.CategoryId);
            return View(productViewModel);
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel productViewModel)
        {
            Product product = new Product()
            {
                Id = productViewModel.Id,
                Title = productViewModel.Title,
                Description = productViewModel.Description,
                Price = productViewModel.Price,
                Discount = productViewModel.Discount,
                CategoryId = productViewModel.CategoryId
            };
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        //Product Images Added Start
        public ActionResult Pictures(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productImage = db.ProductImages.Where(c => c.ProductId == id).ToList();
            ViewBag.ImageList = productImage;
            ProductImageViewModel productImageViewModel = new ProductImageViewModel();
            productImageViewModel.ProductId = (int)id;
            return View(productImageViewModel);
        }

        [HttpPost]
        public ActionResult Pictures(ProductImageViewModel productImageViewModel)
        {
            var productImageCount = db.ProductImages.Where(c => c.ProductId == productImageViewModel.ProductId).ToList().Count();
            if (productImageCount > 4)
            {
                return RedirectToAction("Pictures");
            }
            string fileName = Path.GetFileNameWithoutExtension(productImageViewModel.Image.FileName);
            string extension = Path.GetExtension(productImageViewModel.Image.FileName);
            var fileNames = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
            string path = fileName + DateTime.Now.ToString("yy-mm-dd") + extension;
            fileName = Path.Combine(Server.MapPath("~/ProductImage/"), fileNames);
            productImageViewModel.Image.SaveAs(fileName);
            ResizeSettings imageResizer = new ResizeSettings()
            {
                Width = 720,
                Height = 398
            };
            ImageBuilder.Current.Build(fileName, fileName, imageResizer);

            ProductImage productImage = new ProductImage()
            {
                ImagePath = path,
                ProductId = productImageViewModel.Id
            };



            db.ProductImages.Add(productImage);
            db.SaveChanges();            
            return RedirectToAction("Pictures");
        }

        public ActionResult Remove(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productImage = db.ProductImages.FirstOrDefault(c => c.Id == id);
            db.Entry(productImage).State = EntityState.Deleted;
            db.SaveChanges();
            var result = "Successfully Deleted";
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //Product Images Added End

    }
}