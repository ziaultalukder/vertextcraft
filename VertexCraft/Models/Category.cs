﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CatId { get; set; }
        public Category Cat { get; set; }
        public bool IsDeleted { get; set; }
    }
}