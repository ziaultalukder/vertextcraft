﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.Models
{
    public class Banner
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string ImagePath { get; set; }
        public bool IsDelete { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}