﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.Models
{
    public class NewArrival
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string FilePath { get; set; }
    }
}