﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public bool IsDelete  { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
    }
}