namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BannerModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Banners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImagePath = c.String(),
                        CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: false)
                .Index(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Banners", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Banners", new[] { "CategoryId" });
            DropTable("dbo.Banners");
        }
    }
}
