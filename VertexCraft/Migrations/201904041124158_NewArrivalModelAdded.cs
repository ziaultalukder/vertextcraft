namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewArrivalModelAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NewArrivals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FilePath = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.NewArrivals");
        }
    }
}
