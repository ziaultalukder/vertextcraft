namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BannerModelAddedChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banners", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Banners", "IsDelete");
        }
    }
}
