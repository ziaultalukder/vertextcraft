namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoryTableAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CatId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CatId, cascadeDelete: false)
                .Index(t => t.CatId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Categories", "CatId", "dbo.Categories");
            DropIndex("dbo.Categories", new[] { "CatId" });
            DropTable("dbo.Categories");
        }
    }
}
