namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductModelChanges : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ProductImages", "IsDeleted");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductImages", "IsDeleted", c => c.Boolean(nullable: false));
        }
    }
}
