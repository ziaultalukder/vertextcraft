namespace VertexCraft.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BannerModelChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Banners", "Title", c => c.String());
            AddColumn("dbo.Banners", "SubTitle", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Banners", "SubTitle");
            DropColumn("dbo.Banners", "Title");
        }
    }
}
