﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<BannerViewModel> BannerViewModels { get; set; }
        public IEnumerable<NewArrivaleViewModel> NewArrivales { get; set; }
        public IEnumerable<ProductNameDiscountViewModel> productNameDiscountViewModels { get; set; }
        public IEnumerable<ProductFeatureArrivalsViewModel> ProductFeatureArrivalsViewModels { get; set; }
    }
}