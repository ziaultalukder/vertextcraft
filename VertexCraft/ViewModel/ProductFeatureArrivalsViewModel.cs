﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.ViewModel
{
    public class ProductFeatureArrivalsViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
    }
}