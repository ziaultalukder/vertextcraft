﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VertexCraft.Models;

namespace VertexCraft.ViewModel
{
    public class ProductImageList
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}