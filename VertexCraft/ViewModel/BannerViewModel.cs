﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VertexCraft.Models;

namespace VertexCraft.ViewModel
{
    public class BannerViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        [Display(Name = "Image")]
        public string ImagePath { get; set; }

        [Required]
        [Display(Name ="Category")]
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public List<Category> Categories { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}