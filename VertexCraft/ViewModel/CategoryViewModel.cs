﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VertexCraft.Models;

namespace VertexCraft.ViewModel
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CatId { get; set; }
        public Category Cat { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}