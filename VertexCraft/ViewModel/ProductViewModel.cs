﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using VertexCraft.Models;

namespace VertexCraft.ViewModel
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        public string ImagePath { get; set; }

        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        public decimal Discount { get; set; }

        public List<ProductImageViewModel> productImageViewModels { get; set; }

        public int CategoryId { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public Category Category { get; set; }
    }
}