﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VertexCraft.ViewModel
{
    public class ProductNameDiscountViewModel
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string Title { get; set; }
        public decimal Discount { get; set; }
    }
}