﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VertexCraft.ViewModel
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }  
        public string Contact { get; set; }  
        public string Email { get; set; }  
        public string Address { get; set; }  
        public string UserName { get; set; }  
        public string RoleName { get; set; }

        public string RoleId { get; set; }
        public List<SelectListItem> ApplicationRole { get; set; }
    }
}