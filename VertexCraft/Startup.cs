﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VertexCraft.Startup))]
namespace VertexCraft
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
